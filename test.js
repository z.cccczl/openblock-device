const OpenBlockDevice = require('./index');

const deviceServer = new OpenBlockDevice();
deviceServer.checkForUpdates().then(sta => {
    console.log(`new version detected?: ${sta}`);

    if (sta) {
        deviceServer.update();
    }
});
