const formatMessage = require('format-message');
const osLocale = require('os-locale');
const express = require('express');
const requireAll = require('require-all');
const Emitter = require('events');
const path = require('path');
const fs = require('fs');
const copydir = require('copy-dir');
const releaseDownloader = require('@fohlen/github-release-downloader');
const ghdownload = require('github-download');
const rimraf = require('rimraf');
const compareVersions = require('compare-versions');

/**
 * Configuration the default user data path.
 * @readonly
 */
const DEFAULT_USER_DATA_PATH = path.join(__dirname, '../.openblockData');

/**
 * Configuration the default port.
 * @readonly
 */
const DEFAULT_PORT = 20122;

/**
 * Configuration the default language.
 * @readonly
 */
const DEFAULT_LANGUAGE = 'en';

/**
 * A server to provide local devices resource.
 */
class OpenBlockDevice extends Emitter{

    /**
     * Construct a OpenBlock device server object.
     * @param {string} userDataPath - the path of user data.
     * @param {string} devicesPath - the path of initial devices data.
     */
    constructor (userDataPath, devicesPath) {
        super();

        if (userDataPath) {
            this._userDataPath = path.join(userDataPath, 'devices');
        } else {
            this._userDataPath = path.join(DEFAULT_USER_DATA_PATH, 'devices');
        }

        if (devicesPath) {
            this._devicesPath = devicesPath;
        } else {
            this._devicesPath = path.join(__dirname, 'devices');
        }

        this._socketPort = DEFAULT_PORT;
        this._locale = DEFAULT_LANGUAGE;

        if (this.checkFirstRun()) {
            this.copyToUserDataPath();
        }
    }

    checkFirstRun () {
        if (!fs.existsSync(this._userDataPath)) {
            console.log(`First start, copy devices file to ${this._userDataPath}`);
            return true;
        }
        return false;
    }

    copyToUserDataPath () {
        if (!fs.existsSync(this._userDataPath)) {
            fs.mkdirSync(this._userDataPath, {recursive: true});
        }
        copydir.sync(this._devicesPath, this._userDataPath, {utimes: true, mode: true});
    }

    setLocale () {
        return new Promise(resolve => {
            osLocale().then(locale => {
                if (locale === 'zh-CN') {
                    this._locale = 'zh-cn';
                } else if (locale === 'zh-TW') {
                    this._locale = 'zh-tw';
                } else {
                    this._locale = locale;
                }

                console.log('set locale:', this._locale);

                formatMessage.setup({
                    locale: this._locale,
                    // eslint-disable-next-line global-require
                    translations: require(path.join(this._userDataPath, 'locales.js'))
                });
                return resolve();
            });
        });
    }

    checkForUpdates () {
        this._configPath = path.join(this._userDataPath, 'config.json');

        if (fs.existsSync(this._configPath)) {
            // eslint-disable-next-line global-require
            this._config = require(this._configPath);

            return new Promise(resolve => {
                releaseDownloader.getReleaseList(`${this._config.user}/${this._config.repo}`).then(release => {
                    this._latestVersion = release[0].tag_name;
                    const curentVersion = this._config.version;

                    resolve(compareVersions.compare(this._latestVersion, curentVersion, '>'));
                });
            }).catch(err => {
                console.error(err);
            });
        }
        return Promise.resolve(false);
    }

    update () {
        rimraf(this._userDataPath, () => {
            ghdownload({user: this._config.user, repo: this._config.repo, ref: this._latestVersion}, this._userDataPath)
                .on('error', err => {
                    console.error(`error while downloading ${this._config.user}/` +
                        `${this._config.repo} ${this._latestVersion}:`, err);
                })
                .on('zip', zipUrl => {
                    console.log(`${zipUrl} downloading...`);
                })
                .on('end', () => {
                    console.log('finish');

                    this._config.version = this._latestVersion;
                    fs.writeFileSync(this._configPath, JSON.stringify(this._config));
                });
        });
    }

    /**
     * Start a server listening for connections.
     * @param {number} port - the port to listen.
     */
    listen (port) {
        if (port) {
            this._socketPort = port;
        }

        this.setLocale().then(() => {
            const devices = requireAll({
                dirname: `${this._userDataPath}`,
                filter: /index.js$/,
                recursive: true
            });

            const devicesThumbnailData = [];

            // eslint-disable-next-line global-require
            const deviceList = require(path.join(this._userDataPath, 'device.js'));
            deviceList.forEach(listItem => {
                let matched = false;
                Object.entries(devices).forEach(catlog => {
                    Object.entries(catlog[1]).forEach(dev => {
                        const content = dev[1]['index.js'](formatMessage);
                        if (content.deviceId === listItem) {
                            const basePath = path.join(catlog[0], dev[0]);

                            if (content.iconURL) {
                                content.iconURL = path.join(basePath, content.iconURL);
                            }
                            if (content.connectionIconURL) {
                                content.connectionIconURL = path.join(basePath, content.connectionIconURL);
                            }
                            if (content.connectionSmallIconURL) {
                                content.connectionSmallIconURL = path.join(basePath, content.connectionSmallIconURL);
                            }
                            matched = true;
                            devicesThumbnailData.push(content);
                        }
                    });
                });
                if (!matched) {
                    devicesThumbnailData.push({deviceId: listItem});
                }
            });

            this._app = express();

            this._app.use((req, res, next) => {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
                next();
            });
            this._app.use(express.static(`${this._userDataPath}`));

            this._app.get('/', (req, res) => {
                res.send(JSON.stringify(devicesThumbnailData));
            });

            this._app.listen(this._socketPort);

            this.emit('ready');
            console.log('socket server listend:', `http://0.0.0.0:${this._socketPort}`);
        });
    }
}

module.exports = OpenBlockDevice;
