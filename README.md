# openblock-device

![](https://img.shields.io/travis/com/openblockcc/openblock-device) ![](https://img.shields.io/github/license/openblockcc/openblock-device)

Provide a local device server for scrachhw.

### Instructions

```bash
npm install
npm start
```

